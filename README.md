# Named-Entity Recognition service
## O-Dev internship test task
The task is to come up with a Named-Entity Recognition service, which would output a list of persons given a text.

My solution is a simple Flask server, wrapped in Docker-compose for easy loading.
I chose not to do this with `Docker build` because it would mean downloading language dictionaries on every launch, which is a big time overhead.

### Requirements:
- Docker

### To launch:
- `git clone https://gitlab.com/Sariell/o-dev-test-task`
- `docker-compose up`
- After that, the server will be available at `127.0.0.1:5000`

### How to use:
Since this is a Flask server, we will send our requests via REST API.

The only enpoint of this url is `/predict` with following GET parameters:
- `language`  - a language, in which the sentence is given. Examples:
    - English
    - RuSsIaN
- `sentence` - the sentence itself, which needs to be processed for named-entities. Examples: 
    - Donald Trump had a meeting with Vladimir Putin yesterday. There were drinking vodka and visiting banya all night long. Alexander Lukashenko was offended that he was not in a party.
    - Donald Trump hatte gestern ein Treffen mit Wladimir Putin. Es gab die ganze Nacht Wodka zu trinken und Banja zu besuchen. Alexander Lukaschenko war beleidigt, dass er nicht auf einer Party war.
    - Путин сыграл Путина в фильме про Путина

Thus, our request will look something like this:

`127.0.0.1:5000/predict?language=russian&sentence=Путин сыграл Путина в фильме про Путина`

### Examples of usage
- **Example #1**:
    - language: `English`
    - sentence: *Donald Trump had a meeting with Vladimir Putin yesterday. There were drinking vodka and visiting banya all night long. Alexander Lukashenko was offended that he was not in a party.*
    - response: `["Donald Trump", "Vladimir Putin", "Alexander Lukashenko"]`
- **Example #2**:
    - language: `German`
    - sentence: *Donald Trump hatte gestern ein Treffen mit Wladimir Putin. Es gab die ganze Nacht Wodka zu trinken und Banja zu besuchen. Alexander Lukaschenko war beleidigt, dass er nicht auf einer Party war.*
    - response: `["Donald Trump", "Wladimir Putin", "Alexander Lukaschenko"]`
- **Example #3**:
    - language: `Russian`
    - sentence: *Путин сыграл Путина в фильме про Путина*
    - response: `[Путин, Путина, Путина]`
- **Example #4**:
    - language: `Russian`
    - sentence: *Дональд Трамп вчера встретился с Владимиром Путиным. Всю ночь пили водку и посещали баню. Александр Лукашенко обиделся, что его не было в партии.*
    - response: `["Дональд Трамп", "Владимиром Путиным", "Александр Лукашенко"]`