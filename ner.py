from typing import List

import spacy

LANGUAGE_DICTIONARIES = {
    'english': "en_core_web_sm",
    'other': "xx_ent_wiki_sm"
}


class NER:
    def ner(self, sentence: str, nlp: spacy) -> List[str]:
        """

        :param sentence
        :param nlp: spacy.lang submodule, a loaded dictionary of languages
        :return:
        """
        ners = []
        parsed = nlp(sentence)
        for ne in parsed.ents:
            # _label for persons is "PERSON" in all dictionaries,
            # except that it is "PER" in multi-language dictionary.
            if ne.label_ == 'PERSON' or ne.label_ == "PER":
                ners.append(ne.text)
        return ners

    def predict(self, sentence: str, language: str) -> List[str]:
        """
        A method that dynamically install required language-dictionary libraries and uses them
        to classify NER's in given sentence.

        :param sentence
        :param language
        :return: list of found entities
        """
        language = LANGUAGE_DICTIONARIES.get(language.lower(), None)
        if language is None:
            # if language not found in Spacy dictionaries, use the multi-language dictionary
            language = LANGUAGE_DICTIONARIES['other']

        try:
            # try loading the dictionary
            nlp = spacy.load(language)
        except OSError:
            # if dictionary is not present locally..
            print("Failed to find language model locally. Pulling..")
            # ..then dynamically install it..
            import subprocess
            subprocess.run(["python3", "-m", "spacy", "download", language])
            # ..and import it
            exec(f"import {language}")
            # and finally load it
            nlp = spacy.load(language)

        return self.ner(sentence, nlp)
