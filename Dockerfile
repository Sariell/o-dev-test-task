FROM python:latest
RUN mkdir /app
WORKDIR /app

EXPOSE 5000

COPY requirements.txt .
RUN pip install -r requirements.txt
RUN python -m spacy download en_core_web_sm
RUN python -m spacy download xx_ent_wiki_sm
COPY . .
CMD flask run --host=0.0.0.0