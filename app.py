from flask import Flask, request, jsonify

from ner import NER

app = Flask(__name__)


@app.route('/predict/', methods=["GET"])
def predict():
    lang = request.args.get('language')
    sentence = request.args.get('sentence')

    ne = NER().predict(sentence, lang)
    return jsonify({"person_names": ne})


if __name__ == '__main__':
    app.run()
